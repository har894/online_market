import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {NavbarComponent} from './header/navbar/navbar.component';
import {NavigationComponent} from './content/navigation/navigation.component';
import {ContentComponent} from './content/content.component';
import { BasketComponent } from './content/basket/basket.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavbarComponent,
    NavigationComponent,
    ContentComponent,
    BasketComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
